### About

A simple SDDM theme inspired on FBSD SLiM theme.

### Preview!
![SDDM Gentoo Black Theme](https://bytebucket.org/rigoletto-freebsd/sddm-gentoo-black-theme/raw/4397ba270c4cd97a346b1b78eff3dd3408dbce5a/src/screenshot.png)

### Installation
```shell
git clone https://bitbucket.org/rigoletto-freebsd/sddm-gentoo-black-theme.git
cp -R sddm-gentoo-black-theme/src /usr/share/sddm/themes/sddm-gentoo-black-theme
```

- Open up `/etc/sddm.conf` file and set `sddm-gentoo-black-theme` as your
  current theme.

```shell
[Theme]
# Current theme name
Current=sddm-gentoo-black-theme
```

### Configuration
- The theme uses the Liberation Sans font by default, but you can change it
  editing the `/usr/share/sddm/themes/sddm-gentoo-black-theme/theme.conf`
  file and setting the desired font in the `displayFont` variable.

```shell
[General]
background=background.png
displayFont="Liberation Sans"
```
